var request = require("supertest");
var auth = require("./token");

// SIGNUP
// positive
describe("User signup", function () {
  it("user signup", async () => {
    const response = await request(auth.url).post("/signup").send({
      userName: "dipatnshu",
      email: "diptanh399@gmail.com",
      password: "deep@12",
      confirmPassword: "deep@12",
      role: "ADMIN",
    });
    expect(response.status).toBe(200);
  });
  it("user login", async () => {
    const response = await request(auth.url).post("/login").send({
      email: "diptanh399@gmail.com",
      password: "deep@12",
    });
    expect(response.status).toBe(200);
    auth.id = response._body.result.id;
    console.log(auth.id);
  });

  // neagtive .... for same username
  it("user signup", async () => {
    const response = await request(auth.url).post("/signup").send({
      userName: "dipatnshu",
      email: "deep25989@gmail.com",
      password: "1345", //BUG
      confirmPassword: "1345",
      role: "ADMIN",
    });
    expect(response.status).not.toBe(200);
  });

  //   // negative ... for space in username
  it("user signup", async () => {
    const response = await request(auth.url).post("/signup").send({
      userName: "kishori dekate",
      email: "kis266919@gmail",
      password: "1203",
      confirmPassword: "1203",
      role: "ADMIN",
    });
    expect(response.status).not.toBe(200);
  });

  // negative .. for email
  it("user signup", async () => {
    const response = await request(auth.url).post("/signup").send({
      userName: "komal",
      email: "abcd",
      password: "deep",
      confirmPassword: "deep@12",
      role: "ADMIN",
    });
    expect(response.status).not.toBe(200);
  });

  //   // negative .. for email
  it("user signup", async () => {
    const response = await request(auth.url).post("/signup").send({
      userName: "nikita",
      email: "abcd@",
      password: "d@12",
      confirmPassword: "d@12",
      role: "ADMIN",
    });
    expect(response.status).not.toBe(200);
  });

  //   // negative .. for email
  it("user signup", async () => {
    const response = await request(auth.url).post("/signup").send({
      userName: "viraj",
      email: "viraj.anything",
      password: "deep@12",
      confirmPassword: "deep@12",
      role: "ADMIN",
    });
    expect(response.status).not.toBe(200);
  });

  //   // // negative .. for dissimilar psw
  it("user signup", async () => {
    const response = await request(auth.url).post("/signup").send({
      userName: "vivek",
      email: "vvek66996@gmail.com",
      password: "vivek@12",
      confirmPassword: "viv@12",
      role: "ADMIN",
    });
    expect(response.status).not.toBe(200);
  });

  // negative .. for psw .....bug
  it("user signup", async () => {
    const response = await request(auth.url).post("/signup").send({
      userName: "tushar",
      email: "tshr59691@gmail.com", //BUG
      password: "1",
      confirmPassword: "1",
      role: "ADMIN",
    });
    expect(response.status).not.toBe(200);
  });
});

// // // LOGIN
// // // positive
describe("login", function () {
  it("user login", async () => {
    const response = await request(auth.url).post("/login").send({
      email: "superadmin@gmail.com",
      password: "admin@1234",
    });
    expect(response.status).toBe(200);
    auth.token = response._body.result.token;
  });

  //   // negative
  it("user login", async () => {
    const response = await request(auth.url).post("/login").send({
      email: "superadmin@gmail.com",
      password: "admin",
    });
    expect(response.status).not.toBe(200);
  });

  //   // negative
  it("user login", async () => {
    const response = await request(auth.url).post("/login").send({
      email: "super@gmail.com",
      password: "admin@1234",
    });
    expect(response.status).not.toBe(200);
  });
});

// // GETALLUSER
// // positive
describe("get all user ", function () {
  it("user getAllUsers", async () => {
    const response = await request(auth.url)
      .get("/getAllUsers?page=1&pagesize=22")
      .set("Authorization", "bearer " + auth.token);
    expect(response.status).toBe(200);
  });

  //   // negative
  it("user getAllUsers", async () => {
    const response = await request(auth.url)
      .get("g/getAllUsers?page=1&pagesize")
      .set("Authorization", "bearer " + auth.token);
    expect(response.status).toBe(404);
  });
});

// // forget psw
// // postive
describe("Forget user ", function () {
  it("user forget", async () => {
    const response = await request(auth.url).post("/forgot-password").send({
      email: "adityar.cse20@sbjit.edu.in",
    });
    expect(response.status).toBe(200);
  });

  //   // negative
  it("user forget", async () => {
    const response = await request(auth.url).post("/forgot-password").send({
      email: "dhananjaynasare@gmail.com",
    });
    expect(response.status).not.toBe(200);
  });
});

// EDIT
// positive
describe("Edit", function () {
  it("user edit", async () => {
    const response = await request(auth.url_1)
      .put("studentDetails/updateStudentDetails/77 ")
      .set("Authorization", "bearer " + auth.token)
      .field("key", "student")
      .field("name", "Salman Khan24")
      .field("dob", "2002-1-16")
      .field("email", "salmank@gmail.com")
      .field("mobile", "6578912400")
      .field("gender", "Male")
      .field("stateId", "21")
      .field("districtId", "386")
      .field("aadharNo", "457104975100")
      .field("status ", "Active")
      .field("hiredStatus", "false")
      .field("talukaId", "173")
      .field("role", "STUDENT")
      .field("planPurchaseDate", "2022-9-8");

    expect(response.status).toBe(200);
  });

  // // negative
  it("user edit", async () => {
    const response = await request(auth.url_1)
      .put("studentDetails/updateStudentDetails/8872")
      .set("Authorization", "bearer " + auth.token)
      .field("key", "teacher")
      .field("name", "Salman Khan24")
      .field("dob", "2002-1-16")
      .field("email", "salmank@gmail.com")
      .field("mobile", "6578912400")
      .field("gender", "Male")
      .field("stateId", "21")
      .field("districtId", "386")
      .field("aadharNo", "457104975100")
      .field("status ", "Active")
      .field("hiredStatus", "false")
      .field("talukaId", "173")
      .field("role", "STUDENT")
      .field("planPurchaseDate", "2022-9-8");

    expect(response.status).not.toBe(200);
  });
});

// get user by id
// positive
describe("get user by profile id", function () {
  it("get profile by id", async () => {
    const response = await request(auth.url)
      .get("/profile?id=210")
      .set("Authorization", "bearer " + auth.token);
    expect(response.status).toBe(200);
  });

  // negative
  it("get profile by id", async () => {
    const response = await request(auth.url)
      .get("/profile?id=21000")
      .set("Authorization", "bearer " + auth.token);
    expect(response.status).not.toBe(200);
  });
});

// DELETE
// positive
describe("Delete user ", function () {
  it("Delete Users", async () => {
    const response = await request(auth.url)
      .delete(`/delete/${auth.id}`)
      .set("Authorization", "bearer " + auth.token);

    expect(response.status).toBe(200);
  });

  // // negative
  it("Delete Users", async () => {
    const response = await request(auth.url_1)
      .delete("studentDetails/deleteStudentDetails/5572")
      .set("Authorization", "bearer " + auth.token);

    expect(response.status).toBe(500);
  });
});
