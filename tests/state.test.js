var request = require("supertest");
var auth = require("./token");

describe("login", function () {
  it("user login", async () => {
    const response = await request(auth.url).post("/login").send({
      email: "superadmin@gmail.com",
      password: "admin@1234",
    });
    expect(response.status).toBe(200);
    auth.token = response._body.result.token;
  });
});

//   create state
//   positive
describe(" Add state", function () {
  it("post add state", async () => {
    const response = await request(auth.url_1)
      .post("state/createState")
      .set("Authorization", "bearer " + auth.token)
      .send([
        {
          stateName: "Andaman and Nicobar Islands",
        },
        {
          stateName: "Andhra Pradesh",
        },
        {
          stateName: "Arunachal Pradesh",
        },
        {
          stateName: "Assam",
        },
        {
          stateName: "Bihar",
        },
        {
          stateName: "Chandigarh",
        },
        {
          stateName: "Chhattisgarh",
        },
        {
          stateName: "Dadra and Nagar Haveli",
        },
        {
          stateName: "Daman and Diu",
        },
        {
          stateName: "Delhi",
        },
        {
          stateName: "Goa",
        },
        {
          stateName: "Gujarat",
        },
        {
          stateName: "Haryana",
        },
        {
          stateName: "Himachal Pradesh",
        },
        {
          stateName: "Jammu and Kashmir",
        },
        {
          stateName: "Jharkhand",
        },
        {
          stateName: "Karnataka",
        },
        {
          stateName: "Kerala",
        },
        {
          stateName: "Lakshadweep",
        },
        {
          stateName: "Madhya Pradesh",
        },
        {
          stateName: "Maharashtra",
        },
        {
          stateName: "Manipur",
        },
        {
          stateName: "Meghalaya",
        },
        {
          stateName: "Mizoram",
        },
        {
          stateName: "Nagaland",
        },
        {
          stateName: "Odisha",
        },
        {
          stateName: "Puducherry",
        },
        {
          stateName: "Punjab",
        },
        {
          stateName: "Rajasthan",
        },
        {
          stateName: "Sikkim",
        },
        {
          stateName: "Tamil Nadu",
        },
        {
          stateName: "Telangana",
        },
        {
          stateName: "Tripura",
        },
        {
          stateName: "Uttar Pradesh",
        },
        {
          stateName: "Uttarakhand",
        },
        {
          stateName: "West Bengal",
        },
      ]);
    expect(response.status).toBe(200);
  });

  // negative
  it("post add state", async () => {
    const response = await request(auth.url_1)
      .post("state/cate")
      .set("Authorization", "bearer " + auth.token)
      .send([
        {
          stateName: "Andaman and Nicobar Islands",
        },
        {
          stateName: "Andhra Pradesh",
        },
        {
          stateName: "Arunachal Pradesh",
        },
        {
          stateName: "Assam",
        },
        {
          stateName: "Bihar",
        },
        {
          stateName: "Chandigarh",
        },
        {
          stateName: "Chhattisgarh",
        },
        {
          stateName: "Dadra and Nagar Haveli",
        },
        {
          stateName: "Daman and Diu",
        },
        {
          stateName: "Delhi",
        },
        {
          stateName: "Goa",
        },
        {
          stateName: "Gujarat",
        },
        {
          stateName: "Haryana",
        },
        {
          stateName: "Himachal Pradesh",
        },
        {
          stateName: "Jammu and Kashmir",
        },
        {
          stateName: "Jharkhand",
        },
        {
          stateName: "Karnataka",
        },
        {
          stateName: "Kerala",
        },
        {
          stateName: "Lakshadweep",
        },
        {
          stateName: "Madhya Pradesh",
        },
        {
          stateName: "Maharashtra",
        },
        {
          stateName: "Manipur",
        },
        {
          stateName: "Meghalaya",
        },
        {
          stateName: "Mizoram",
        },
        {
          stateName: "Nagaland",
        },
        {
          stateName: "Odisha",
        },
        {
          stateName: "Puducherry",
        },
        {
          stateName: "Punjab",
        },
        {
          stateName: "Rajasthan",
        },
        {
          stateName: "Sikkim",
        },
        {
          stateName: "Tamil Nadu",
        },
        {
          stateName: "Telangana",
        },
        {
          stateName: "Tripura",
        },
        {
          stateName: "Uttar Pradesh",
        },
        {
          stateName: "Uttarakhand",
        },
        {
          stateName: "West Bengal",
        },
      ]);
    expect(response.status).toBe(404);
  });
});

//   GETBYID
//   positive
describe("State get by ID", function () {
  it("get by id", async () => {
    const response = await request(auth.url_1)
      .get("/state/getById/21")
      .set("Authorization", "bearer " + auth.token);
    expect(response.status).toBe(200);
  });
  //   negative
  it("get by id", async () => {
    const response = await request(auth.url_1)
      .get("/state/getById/48000000")
      .set("Authorization", "bearer " + auth.token);
    expect(response.status).toBe(400);
  });
});

// GETALL
// positive
describe("State get all", function () {
  it("get all", async () => {
    const response = await request(auth.url_1)
      .get("/state/getAllStateListing?page=1&pagesize=10")
      .set("Authorization", "bearer " + auth.token);
    expect(response.status).toBe(200);
  });

  // negative
  it("get all", async () => {
    const response = await request(auth.url)
      .get("/state/getAllStateListing?page=1&pagesize100089")
      .set("Authorization", "bearer " + auth.token);
    expect(response.status).not.toBe(200);
  });

  it("get all", async () => {
    const response = await request(auth.url_1)
      .get("/district/getDistrictByStateId")
      .set("Authorization", "bearer " + auth.token);
    expect(response.status).not.toBe(200);
  });
});
