var auth = require("./tests/token");
const supertest = require("supertest");
const url = "http://156.67.219.134:6060/api/v1/admin/user";

module.exports = async () => {
  // part 1 start

  let user = {
    userName: "adi",
    email: "adi@gmail.com",
    password: "adi@123",
    role: "SUPER ADMIN",
  };

  let data = await supertest(url)
    .post("/signup")
    .set("content-Type", "application/json")
    .send(user);

  if (data.statusCode == 200) {
    console.log("config login");
    let log = await supertest(url)
      .post("/login")
      .set("Content-Type", "application/json")
      .send({
        email: "adiram@gmail.com",
        password: "adiram@123",
      });
    console.log(log);

    auth.token = log.body.result.token;
    console.log("jest file==> ", process.env.TOKEN);
  }

  // part 1 end

  // part 2  start

  // let a = await supertest(url)
  //   .post("/login")
  //   .set("content-Type", "application/json")
  //   .send({
  //     email: "adiram@gmail.com",
  //     password: "adiram@123",
  //   });

  // let response = await supertest(url)
  //   .post("/login")
  //   .set("content-Type", "application/json")
  //   .send({
  //     email: "adiram@gmail.com",
  //     password: "adiram@123",
  //   });
  // auth.token = response._body.result.token;
  // console.log(auth.token);

  // auth.token = a._body.result.token;

  // console.log(auth.token);

  // part 2 end

  return (
    {
      verbose: true,
      testTimeout: 50000,
    },
    (jest = {
      reporters: [
        "default",
        [
          "./node_modules/jest-html-reporters",
          {
            pageTitle: "Test Report",
          },
        ],
      ],
    })
  );
};
